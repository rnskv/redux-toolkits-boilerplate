import Express from 'express';
import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import {
  ServerStyleSheet,
  StyleSheetManager,
} from 'styled-components';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import routes from 'configs/routes';
import rootReducer from 'client/core/store/rootReducer.js';

const app = Express();

app.use('/static', Express.static('dist'));
app.use('/assets', Express.static('dist/public/assets'));
app.use(handleRender);

function handleRender(req, res) {
  const sheet = new ServerStyleSheet();
  const preloadedState = { theme: { name: 'default' } };
  const store = createStore(rootReducer, preloadedState);
  const context = {};
  let html = '';
  let styleTags = '';
  try {
    html = renderToString(
      <StyleSheetManager sheet={sheet.instance}>
        <Provider store={store}>
          <StaticRouter location={req.url} context={context}>
            {renderRoutes(routes)}
          </StaticRouter>
        </Provider>
      </StyleSheetManager>,
    );
    styleTags = sheet.getStyleTags();
    sheet.seal();
  } catch (error) {
    console.error(error);
  } finally {
    sheet.seal();
  }
  const finalState = store.getState();
  res.send(renderFullPage(html, finalState, styleTags));
}
function renderFullPage(html, preloadedState, styleTags) {
  return `
    <!doctype html>
    <html>
      <head>
        <title>Redux Universal Example</title>
        ${styleTags}
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // https://redux.js.org/recipes/server-rendering/#security-considerations
          window.__PRELOADED_STATE__ = ${JSON.stringify(
            preloadedState,
          ).replace(/</g, '\\u003c')}
        </script>
        <script src="/static/client-app.js"></script>
      </body>
    </html>
    `;
}

app.listen(CONFIG.SERVER_PORT, () => {
  console.log(`Server started at port: ${CONFIG.SERVER_PORT}`);
});
