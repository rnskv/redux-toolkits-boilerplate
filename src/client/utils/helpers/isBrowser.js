export default () =>
  typeof window !== 'undefined' && window.window === window;
