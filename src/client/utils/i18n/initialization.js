import i18n from 'i18next';
import HttpApi from 'i18next-http-backend';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

const nameSpaces = ['common', 'theme'];
const languages = ['en'];

function init() {
  i18n
    .use(HttpApi)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
      defaultNS: 'common',
      defaultLng: 'en',
      fallbackNS: 'common',
      fallbackLng: 'en',
      supportedLngs: languages,
      ns: nameSpaces,
      debug: true,
      backend: {
        loadPath: '/assets/locales/{{lng}}/{{ns}}.json',
      },
    });
}

export default init;
