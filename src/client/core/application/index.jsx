import { hot } from 'react-hot-loader/root';
import React from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import routes from 'configs/routes';
import store from 'core/store';
import history from 'utils/helpers/history';

const Application = () => {
  return (
    <Provider store={store}>
      <Router history={history}>{renderRoutes(routes)}</Router>
    </Provider>
  );
};

export default hot(Application);
