import PropTypes from 'prop-types';
import React, { Suspense } from 'react';
import { useSelector } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { ThemeProvider } from 'styled-components';
import { getName as getThemeName } from 'features/theme/selectors';

const Loader = () => <div>Main loader...</div>;

const Root = ({ route }) => {
  const { routes } = route;
  const themeName = useSelector(getThemeName);

  if (typeof window === 'undefined') {
    return <Loader />;
  }

  return (
    <ThemeProvider theme={{ name: themeName || 'default' }}>
      <Suspense fallback={<Loader />}>
        {renderRoutes(routes)}
      </Suspense>
    </ThemeProvider>
  );
};

Root.propTypes = {
  route: PropTypes.shape({
    routes: PropTypes.array.isRequired,
  }),
};

export default Root;
