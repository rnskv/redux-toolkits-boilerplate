import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';
import thunkMiddleware from 'redux-thunk';
const preloadedState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

const store = configureStore({
  reducer: rootReducer,
  middleware: [thunkMiddleware],
  preloadedState,
});

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./rootReducer', () => {
    const newRootReducer = require('./rootReducer').default;
    store.replaceReducer(newRootReducer);
  });
}

export default store;
