import { combineReducers } from '@reduxjs/toolkit';

import { reducers } from 'features';

export default combineReducers(reducers);
