import React from 'react';
import Application from './application';
import initI18N from 'utils/i18n/initialization';
import { hydrate, render } from 'react-dom';
import isBrowser from 'utils/helpers/isBrowser';

initI18N();

const renderMethod = isBrowser() ? render : hydrate;

renderMethod(<Application />, document.getElementById('root'));
