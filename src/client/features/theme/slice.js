import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  name: 'default',
};

const loginSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    toggleTheme(state, action) {
      const reverse = {
        default: 'dark',
        dark: 'default',
      };

      state.name = reverse[state.name];
    },
  },
});

export const { toggleTheme } = loginSlice.actions;

export default loginSlice.reducer;
