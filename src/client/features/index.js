import loginReducer from './login/slice';
import themeReducer from './theme/slice';

export const reducers = {
  login: loginReducer,
  theme: themeReducer,
};
