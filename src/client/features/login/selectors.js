export const getName = (state) => state.login.name;
export const getLoadingState = (state) => state.login.isLoading;
