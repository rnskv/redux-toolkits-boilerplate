import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  name: '',
  token: '',
  error: '',
  isLoading: false,
};

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    setName(state, action) {
      const { name } = action.payload;
      state.name = name;
    },
    loginStart(state, action) {
      state.isLoading = true;
    },
    loginSuccess(state, action) {
      state.isLoading = false;
      state.token = 'jwttokenfromserver';
    },
    loginFailure(state, action) {
      state.isLoading = false;
      state.error = 'some error';
    },
  },
});

export const {
  setName,
  loginStart,
  loginSuccess,
  loginFailure,
} = loginSlice.actions;

export const login = (payload) => async (dispatch) => {
  dispatch(loginStart());

  setTimeout(() => {
    dispatch(loginSuccess());
  }, 1000);
};

export default loginSlice.reducer;
