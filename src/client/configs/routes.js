import React from 'react';
import isBrowser from 'utils/helpers/isBrowser';

import Root from 'core/application/Root';
const components = {};

if (isBrowser()) {
  components.Home = React.lazy(() => import('ui/pages/home'));
  components.Login = React.lazy(() => import('ui/pages/login'));
} else {
  components.Home = require('ui/pages/home').default;
  components.Login = require('ui/pages/login').default;
}

export default [
  {
    component: Root,
    routes: [
      {
        path: '/',
        exact: true,
        component: components.Home,
      },
      {
        path: '/login',
        component: components.Login,
      },
    ],
  },
];
