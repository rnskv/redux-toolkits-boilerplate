import PropTypes from 'prop-types';
import React from 'react';
import { StyledButton } from './styled';

const Button = ({ className, style, type, onClick, children }) => {
  return (
    <StyledButton
      style={style}
      className={className}
      type={type}
      onClick={onClick}
    >
      {children}
    </StyledButton>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  type: PropTypes.string,
  children: PropTypes.any,
  onClick: PropTypes.func,
};

export default Button;
