import React from 'react';

import { action } from '@storybook/addon-actions';
import Button from './index.jsx';
import { ThemeProvider } from 'styled-components';

export default {
  title: 'Button',
  component: Button,
};

export const Text = () => (
  <Button onClick={action('clicked')}>Hello Button</Button>
);

Text.story = {
  name: 'Simple Button',
};

export const Emoji = () => (
  <ThemeProvider theme={{ name: 'default' }}>
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  </ThemeProvider>
);

Emoji.story = {
  name: 'Emoji Button',
};

export const Yellow = () => (
  <ThemeProvider theme={{ name: 'default' }}>
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        Example button in yellow style
      </span>
    </Button>
  </ThemeProvider>
);

Emoji.story = {
  name: 'Default Theme',
};
