import styled from 'styled-components';
import theme from 'styled-theming';

const bgColor = theme('name', {
  default: '#f6e444',
  dark: 'black',
});

const activeBgColor = theme('name', {
  default: '#fff7af',
  dark: 'white',
});

const color = theme('name', {
  default: '#0c0d11',
  dark: 'white',
});

const activeColor = theme('name', {
  default: '#0c0d11',
  dark: 'black',
});

const focusColor = theme('name', {
  default: 'blue',
  dark: 'black',
});

export const StyledButton = styled.button`
  background: ${bgColor};
  color: ${color};
  padding: 8px 15px;
  outline: none;
  border: 2px solid transparent;
  border-radius: 3px;
  cursor: pointer;
  box-sizing: border-box;
  transition: background .3s
  &:hover {
    background: ${activeBgColor};
    color: ${activeColor};
  }

  &:focus {
    border: 2px solid ${focusColor};
  }
`;
