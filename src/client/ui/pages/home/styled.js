import styled from 'styled-components';
import theme from 'styled-theming';
import { Button } from 'uikit';

const bgColor = theme('name', {
  default: 'black',
  dark: 'white',
});

const color = theme('name', {
  default: 'white',
  dark: 'black',
});

export const Wrapper = styled.div`
  background: ${bgColor};
  color: ${color};
  padding: 50px;
  margin: 0 auto;
  transition: background 0.3s;
`;

export const HelloButton = styled(Button)`
  margin: 0 15px;
`;
