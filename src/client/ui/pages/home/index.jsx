import React from 'react';
import { useDispatch } from 'react-redux';
import { Wrapper, HelloButton } from './styled';
import { toggleTheme } from 'features/theme/slice';
import { useTranslation } from 'react-i18next';

const Home = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const onClick = () => {
    dispatch(toggleTheme());
  };
  return (
    <Wrapper>
      Home page
      <HelloButton onClick={onClick}>{t('theme:toggle')}</HelloButton>
    </Wrapper>
  );
};

export default Home;
