import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { getName, getLoadingState } from 'features/login/selectors';
import { setName, login } from 'features/login/slice';

const Login = () => {
  const { t } = useTranslation();
  const isLoading = useSelector(getLoadingState);
  const name = useSelector(getName);

  const inputRef = useRef(null);
  const dispatch = useDispatch();

  const onSubmit = (e) => {
    e.preventDefault();
    dispatch(login());
    dispatch(setName({ name: inputRef.current.value }));
  };

  return (
    <React.Fragment>
      {isLoading ? 'loading' : ''}
      <div>Login: {name}</div>
      <input ref={inputRef} type="text" />
      <button onClick={onSubmit}>{t('common:hello')}</button>
    </React.Fragment>
  );
};

export default Login;
