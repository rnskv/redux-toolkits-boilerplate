import PropTypes from 'prop-types';
import React from 'react';

const MainTemplate = ({ children }) => {
  return (
    <div>
      <h1>{'Шаблон Main:'}</h1>
      <content>{children}</content>
    </div>
  );
};

MainTemplate.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
};

export default MainTemplate;
