const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = [
  new HtmlWebpackPlugin({
    template: './src/client/public/index.html',
  }),
  new CleanWebpackPlugin({
    cleanOnceBeforeBuildPatterns: ['**/*', '!server*/**'],
    exclude: ['server/*'],
    verbose: true,
  }),
  new CopyPlugin({
    patterns: [
      {
        from: path.resolve(
          __dirname,
          '../../../src/client/public/assets',
        ),
        to: path.resolve(__dirname, '../../../dist/public/assets'),
      },
    ],
  }),
];
