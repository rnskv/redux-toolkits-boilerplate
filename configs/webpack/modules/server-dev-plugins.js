const path = require('path');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = [
  new NodemonPlugin({
    script: path.resolve(__dirname, '../../../dist/server-app.js'),
  }),
];
