const path = require('path');
const webpack = require('webpack');
process.env.NODE_CONFIG_DIR = path.resolve(
  __dirname,
  '../../variables',
);
console.log(require('config'));
module.exports = (env) => [
  new webpack.ProgressPlugin({
    handler: function (percentage, message) {
      process.stdout.write(
        `BUILD:\r${message} ${(percentage * 100).toFixed(0)}%\r`,
      );
    },
  }),
  new webpack.DefinePlugin({
    CONFIG: JSON.stringify(require('config')),
  }),
];
