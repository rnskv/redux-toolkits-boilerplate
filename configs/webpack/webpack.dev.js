const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');

const client = (env) => ({
  mode: 'development',
  devtool: 'inline-source-map',
  resolve: {
    alias: { 'react-dom': '@hot-loader/react-dom' },
  },
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
    historyApiFallback: true,
    hotOnly: true,
    writeToDisk: true,
    overlay: {
      warnings: true,
      errors: true,
    },
    watchOptions: {
      poll: true,
    },
    port: 9000,
  },
});

const server = (env) => ({
  mode: 'development',
  devtool: 'inline-source-map',
});

module.exports = (env) => [
  merge(common(env)[0], client(env)),
  merge(common(env)[1], server(env)),
];
