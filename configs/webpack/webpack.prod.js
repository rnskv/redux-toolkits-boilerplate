const merge = require('webpack-merge');
const common = require('./webpack.common');

const client = (env) => ({
  mode: 'production',
});

const server = (env) => ({
  mode: 'production',
});

module.exports = (env) => [
  merge(common(env)[0], client(env)),
  merge(common(env)[1], server(env)),
];
