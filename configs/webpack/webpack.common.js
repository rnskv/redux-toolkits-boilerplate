const path = require('path');
const clientPlugins = require('./modules/client-plugins');
const rules = require('./modules/rules');
const serverExternals = require('./modules/server-externals');
const bothPlugins = require('./modules/both-plugins');
const serverDevPlugins = require('./modules/server-dev-plugins');

const client = (env) => ({
  entry: './src/client/core/index.jsx',
  target: 'web',
  node: {
    fs: 'empty',
  },
  output: {
    path: path.join(__dirname, '../../dist'),
    filename: 'client-app.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules', './src/client'],
  },
  module: {
    rules,
  },
  plugins: [...clientPlugins, ...bothPlugins(env)],
});

const server = (env) => ({
  entry: './src/server/index.js',
  target: 'node',
  output: {
    path: path.join(__dirname, '../../dist'),
    filename: 'server-app.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules', './src/server', './src/client'],
    alias: {
      client: path.join(__dirname, '../../src/client'),
    },
  },
  module: {
    rules,
  },
  plugins: [...bothPlugins(env), ...serverDevPlugins],
  externals: serverExternals,
});

module.exports = (env) => [client(env), server(env)];
