const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  SERVER_HOST: 'localhost',
  SERVER_PORT: 3000,
  IS_BROWSER:
    typeof window !== 'undefined' && window.window === window,
};
