# Starter kit for new projects
includes:
- Routing by react-router-config;
- Personal server on express;
- Server side rendering;
- Localization by i18n;
- Theming on styled-components;
- React-redux state manager;
- React-toolkits for minimilize code base, and splitting project on features;
- Redux-thunk for asynchronius actions;
- Code linting;
- Hot reloading for all modules;
- Storybook for reusable components;